package com.mainor.notes.persistence;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.util.AttributeSet;

public class FixRecycleView extends RecyclerView
{
    public FixRecycleView(Context context )
    {
        super( context );
    }

    public FixRecycleView(Context context, AttributeSet attrs )
    {
        super( context, attrs );
    }

    public FixRecycleView(Context context, AttributeSet attrs, int defStyle )
    {
        super( context, attrs, defStyle );
    }

    @Override
    public void stopScroll()
    {
        try
        {
            super.stopScroll();
        }
        catch( NullPointerException exception )
        {
            /**
             *  The mLayout has been disposed of before the
             *  RecyclerView and this stops the application
             *  from crashing.
             */
        }
    }
}