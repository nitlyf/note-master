package com.mainor.notes.persistence;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;

import com.mainor.notes.entities.Note;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Database(entities = {Note.class}, version = 1, exportSchema = false)

public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase database;

    public abstract NoteDao noteDao();
    public static AppDatabase getDatabase(Context context) {
        if (database== null) {
            database = Room.databaseBuilder(context.getApplicationContext(),
                    AppDatabase.class, AppDatabase.class.getName())
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return database;
    }

    public static void destroyInstance() {
        database= null;
    }

    public class TimestampConverter {
        String pattern = "dd MMMMM yyyy HH:mm aa";
        DateFormat dateFormat =new SimpleDateFormat("dd MMMMM yyyy HH:mm aa");

        @TypeConverter
        public Date fromTimestamp(String value) {
            if (value != null) {
                try {
                    return dateFormat.parse(value);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                return null;
            } else {
                return null;
            }
        }
    }
}
